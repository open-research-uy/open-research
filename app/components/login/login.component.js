let loginComponent = Vue.component('login-component', function (resolve, reject){
	axios.get('./app/components/login/loginView.html').then( function (view){
		resolve({
			template: view,
			data: function (){
				return {

				}
			},
			methods: {
				verMas: function (item){
					this.seeMore = true;
					this.article = item;
				},
				back: function (){
					this.seeMore = false;
				}
			}
		})
	})
})