const login = loginComponent;
const registro = registroComponent;

const routes = [
  { path: '/', component: login },
  { path: '/registrarse', component: registro }
]

const router = new VueRouter({
  routes
})

let app = new Vue({
  router,
  data: {
		titulo: 'OPEN-RESEARCH',
		footerTitle: "OPEN-RESEARCH",
		arrMenu: [
			{
				name:"Login",
				link:"/"
			},
			{
				name:"Registro",
				link:"/Registrarse"
			}
		],
		currentRoute: window.location.pathname
	}
}).$mount('#app')