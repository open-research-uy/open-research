Vue.component('footer-item', function (resolve, reject){
	axios.get('./app/shared/footerView.html').then( function (view){
		resolve({
			props:['title','year'],
			template: view
		})
	})
})